import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { shallow, mount, render } from 'enzyme';
import FormSection from './../src/FormSection';
import AddButton from './../src/AddButton';
import React from 'react';

describe('FormSection', () => {
    it('is a form element', () => {
        let wrapper = shallow(<FormSection />);
        expect(wrapper.type()).toEqual('form');
    });
    it('renders 4 InputBoxes', () => {
        let wrapper = shallow(<FormSection />);
        expect(wrapper.find('InputBox')).toHaveLength(4);
    });
    it('renders 1 AddButton', () => {
        let wrapper = shallow(<FormSection />);
        expect(wrapper.find('AddButton')).toHaveLength(1);
    });
    it('sets initial state when constructed', () => {
        let formInstance = new FormSection();
        expect(formInstance.state.formData).toEqual({ "contactNumber": "", "email": "", "firstName": "", "surname": "" });
    });
    it('updates state when typing into input boxes', () => {
        let wrapper = shallow(<FormSection />);
        console.log(wrapper.html());

    });
})
