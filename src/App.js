import React from 'react';
import DataSection from './DataSection';
import FormSection from './FormSection'

class App extends React.Component {
    render() {
        return (
            <div>
                <FormSection className="FormSection"/>
                <DataSection className="DataSection"/>
            </div>
        )
    }
};

export default App;
