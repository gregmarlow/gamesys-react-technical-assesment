import React from 'react';

class InputBox extends React.Component {
    render() {
        return (
            <input name={this.props.name} placeholder={this.props.placeholder} type={this.props.type} value={this.props.value} onChange={this.props.handleInput} />
        )
    }
}

export default InputBox;
