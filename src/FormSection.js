import React from 'react';
import InputBox from './InputBox';
import AddButton from './AddButton';
import database from './firebase';

class FormSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getResetState();
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getResetState() {
        return {
            formData: {
                firstName: '',
                surname: '',
                contactNumber: '',
                email: ''
            },
            formErrors: {
                firstName: '',
                surname: '',
                contactNumber: '',
                email: ''
            },
            isValid: {
                firstName: false,
                surname: false,
                contactNumber: false,
                email: false,
                form: false
            }
        }
    }

    handleSubmit(event) {
        let formData = this.state.formData;
        let form = event.target;

        event.preventDefault(); //prevent page refresh
        
        let key = database.ref('/users').push(formData).key; //push to firebase database and return entry key
        console.log(key);

        //reset state and form and clear input boxes
        this.setState(this.getResetState());
        form.reset();
    }

    handleInput(event) {
        let { name, value } = event.target;
        let formData = { ...this.state.formData, [name]: value };

        this.setState({ formData }, () => this.validateField(name, value)); //update state.formData with new input and validate
    }

    validateField(name, value) {
        let newIsValid = this.state.isValid;
        let formErrors = this.state.formErrors;

        switch (name) {
            case 'firstName':
                newIsValid.firstName = value.trim().length > 0 && value.length <= 50 && !(/\s/.test(value));
                formErrors.firstName = newIsValid.firstName ? '' : 'First Name invalid: Must contain no spaces and be less than 50 characters.';
                break;
            case 'surname':
                newIsValid.surname = value.trim().length > 0 && value.length <= 50 && !(/\s/.test(value));
                formErrors.surname = newIsValid.surname ? '' : 'Surname invalid: Must contain no spaces and be less than 50 characters.';
                break;
            case 'contactNumber':
                newIsValid.contactNumber = value.trim().length > 0 && value.length <= 15 && value.length >= 7 && /^\+?\d+$/.test(value);
                formErrors.contactNumber = newIsValid.contactNumber ? '' : 'Contact Number invalid: Must be between 7-15 characters and contain no special characters (except +).';
                break;
            case 'email':
                newIsValid.email = value.trim().length > 0 && /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i.test(value);
                formErrors.email = newIsValid.email ? '' : 'Email invalid: Must be a valid email address.';
                break;
            default:
                break;
        }

        this.setState({ isValid: newIsValid }, this.validateForm); //update state.isValid and validate form
    }

    validateForm() {
        let newIsValid = this.state.isValid;

        newIsValid.form = (newIsValid.firstName && newIsValid.surname && newIsValid.contactNumber && newIsValid.email);
        this.setState({ isValid: newIsValid }); //update state.isValid.form
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className={this.props.className}>
                    <InputBox name="firstName" placeholder="First Name" value={this.state.firstName} type="text" handleInput={this.handleInput} />
                    <InputBox name="surname" placeholder="Surname" value={this.state.surname} type="text" handleInput={this.handleInput} />
                    <InputBox name="contactNumber" placeholder="Contact Number" value={this.state.contactNumber} type="text" handleInput={this.handleInput} />
                    <InputBox name="email" placeholder="Email" value={this.state.email} type="email" handleInput={this.handleInput} />
                    <AddButton disabled={!(this.state.isValid.form)} >Add</AddButton>
                </div>
            </form>

        )
    }
}

export default FormSection;
