import React from 'react';

class AddButton extends React.Component {
    
    render() {
        return (
            <button type="submit" disabled={this.props.disabled}>{this.props.children}</button>
        )
    }
}

export default AddButton;
