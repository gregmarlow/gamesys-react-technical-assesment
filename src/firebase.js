import firebase from 'firebase/app';
import 'firebase/database';

var config = {
    apiKey: "AIzaSyASyVfrzx-tRWe9PJMz30O33NeuPVUQHsI",
    authDomain: "employee-database-projec-6d1eb.firebaseapp.com",
    databaseURL: "https://employee-database-projec-6d1eb.firebaseio.com",
    projectId: "employee-database-projec-6d1eb",
    storageBucket: "",
    messagingSenderId: "986543662694"
};
firebase.initializeApp(config);
var database = firebase.database();

export default database;
